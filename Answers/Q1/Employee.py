# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 10:51:01 2020

@author: user
"""


import sqlite3
conn = sqlite3.connect('students.db') # connect to the database
cursor = conn.cursor() # instantiate a cursor obj
employee= """CREATE TABLE EMPLOYEE1(
        NAME TEXT NOT NULL,
        EMP_ID INTEGER NOT NULL,
        SALARY INTEGER NOT NULL,
        DEPT_ID INTEGER NOT NULL)"""
cursor.execute(employee)
conn.commit()

emp="""ALTER TABLE EMPLOYEE1
    ADD COLUMN CITY TEXT"""
cursor.execute(emp)
conn.commit()

empl="INSERT INTO EMPLOYEE1 (NAME, EMP_ID, SALARY, DEPT_ID, CITY) VALUES(?,?,?,?,?)"

cursor.execute(empl, ('JOHN',1,30000,2,'XYZ'))
cursor.execute(empl, ('JOSE',2,30000,1,'JKL'))
cursor.execute(empl, ('ADAM',3,30000,2,'XYZ'))
cursor.execute(empl, ('RACHEL',4,30000,1,'QWE'))
cursor.execute(empl, ('SAMANTHA',5,30000,3,'MNO'))
conn.commit()

cursor = conn.execute("SELECT * from EMPLOYEE1")
print(cursor.fetchall())
conn.close()