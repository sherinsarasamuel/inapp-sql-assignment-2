# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 12:17:52 2020

@author: user
"""

import sqlite3
conn = sqlite3.connect('students.db') # connect to the database
cursor = conn.cursor() # instantiate a cursor obj


cursor = conn.execute("SELECT * from EMPLOYEE1")
records=cursor.fetchall()
for row in records:
    print("Employee Name: ", row[0])
    print("Employee ID:", row[1])
    print("Salary:", row[2])
    print("Department ID:", row[3])
    print("City:", row[4],"\n")

#printing details of name starting with J
cursor.execute("select NAME, EMP_ID, SALARY from EMPLOYEE1 where NAME like 'J%'")  
  
    #fetching the rows from the cursor object  
result = cursor.fetchall()  
print("Names    id    Salary");  
  
for row in result:  
    print("%s    %d    %d" %(row[0],row[1],row[2]))  

cursor=conn.cursor()
option=input('Do you want to change name? (Y/y)')
if option=='y' or option=='Y':
    new_id=int(input('Enter ID of Employee name to be changed: '))
    name=input('Enter new name: ')
    remote="update EMPLOYEE1 set name = ? where EMP_ID = ?"
    cursor.execute(remote,(name,new_id))
    
cursor=conn.cursor()
cursor = conn.execute("SELECT * from EMPLOYEE1")
print(cursor.fetchall())

