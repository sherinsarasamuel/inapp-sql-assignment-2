# -*- coding: utf-8 -*-
"""
Created on Tue Nov 24 14:37:29 2020

@author: user
"""


import sqlite3
conn = sqlite3.connect('students.db') # connect to the database
cursor = conn.cursor() # instantiate a cursor obj
dept= """CREATE TABLE DEPARTMENTS(
    DEPT_ID INTEGER NOT NULL,
    DEPT_NAME TEXT NOT NULL)"""
cursor.execute(dept)
conn.commit()

dep="INSERT INTO DEPARTMENTS (DEPT_ID,DEPT_NAME) VALUES(?,?)"
cursor.execute(dep,(1,'TAX'))
cursor.execute(dep,(2,'SALES'))
cursor.execute(dep,(3,'MARKETTING'))
cursor.execute(dep,(4,'FINANCE'))
cursor.execute(dep,(5,'HUMAN RESOURCE'))
conn.commit()
conn.close()