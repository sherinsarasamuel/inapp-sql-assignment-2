# InApp SQL Assignment-2

Question 1

Create a Table Employee which will have the four columns, i.e., Name, ID, salary, and Department_id.

- Add a new column ‘City’ to the Table Employee.
- Insert 5 records into this table.
- Read the Name, ID, and Salary from the Employee table and print it.
- Print the details of employees whose names start with ‘j’ (or any letter input by the user)
- Print the details of employees with ID’s inputted by the user.
- Change the name of the employee whose ID is input by the user.



Question 2

Create one more Table called Departments with two columns Department_id and Department_name.

- Insert 5 records into this table.
- Print the details of all the employees in a particular department (Department_id is input by the user)


